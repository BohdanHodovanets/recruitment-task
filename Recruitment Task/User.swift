//
//  User.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/7/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

class User: Codable {
    var livesIn: String?
    var bornOn: String?
    var from: String?
    var studiedAt: String?
    var phoneNumber: String?
    var biography: String?
}
