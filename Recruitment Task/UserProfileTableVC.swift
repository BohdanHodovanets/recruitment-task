//
//  UserProfileTableVC.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/6/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

class UserProfileTableVC: UITableViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var livesInLabel: UILabel!
    @IBOutlet weak var bornOnLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var studiedAtLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var biographyTextView: UITextView!
    
    private let storage = StorageManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createEditBarButton()
        if let url = Constants.url {
            userImage.downloadImage(from: url)
            userImage.setRadius()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configureTable(with: getDataFromStorages())
    }
    
    override func viewDidLayoutSubviews() {
        biographyTextView.setContentOffset(.zero, animated: false)
    }
    
    //MARK: - Navigation
    private func createEditBarButton() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .edit,
                                         target: self,
                                         action: #selector(tappedEditButton(sender:)))
        
        navigationItem.title = Constants.titles.Profile.rawValue //Set tittle for User Profile view
        navigationItem.rightBarButtonItem = saveButton
    }
    
    @objc func tappedEditButton(sender: UIBarButtonItem) {
        guard let editUserProfileTableVC = UIStoryboard.init(name: "Main", bundle: .main).instantiateViewController(withIdentifier: String(describing: EditUserProfileTableVC.self)) as? EditUserProfileTableVC else { return }
        navigationController?.pushViewController(editUserProfileTableVC, animated: true)
        
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.userProfileRows
    }
    
    //MARK: - Storage
    
    private func getDataFromStorages() -> User? {
        let decoder = JSONDecoder()
        
        guard let savedUser = storage.get(key: Constants.keys.userKey) as? Data else { return nil }
        if let user = try? decoder.decode(User.self, from: savedUser) {
            return user
        } else {
            return nil
        }
    }
  
    private func configureTable(with user: User?) {
        guard let user = user,
            let livesIn = user.livesIn,
            let bornOn = user.bornOn,
            let fromCity = user.from,
            let studiedAt =  user.studiedAt,
            let phoneNumber = user.phoneNumber,
            let biography = user.biography else { return }

        livesInLabel.text = Constants.prefix.livesIn.rawValue + livesIn
        bornOnLabel.text = Constants.prefix.bornOn.rawValue + bornOn
        fromLabel.text = Constants.prefix.fromCity.rawValue + fromCity
        studiedAtLabel.text = Constants.prefix.studiedAt.rawValue + studiedAt
        phoneNumberLabel.text = Constants.prefix.phoneNumber.rawValue + phoneNumber
        biographyTextView.text = biography
    }
}
