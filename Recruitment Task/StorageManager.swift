//
//  StorageManager.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/7/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

class StorageManager {
    typealias Keys = Constants.keys
    
    private let storage = UserDefaults.standard
    
    // --- Any value
    func set(value: Any?, key: Keys) {
        storage.set(value, forKey: key.rawValue)
    }
    
    func get(key: Keys) -> Any? {
        let value = storage.object(forKey: key.rawValue)
        return value
    }
}
