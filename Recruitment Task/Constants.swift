//
//  Constants.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/7/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

struct Constants {
    
    //MARK: - Constant properties
    static let maxLengthPhoneNumber = 9
    static let minLengthPhoneNumber = 0
    static let url = URL(string: "https://i.pinimg.com/originals/43/f9/07/43f90790a622f7af320e254686f6243f.jpg")
    static let userProfileRows = 7
    static let editUserProfileRows = 6
    
    //MARK: - Date formate
    enum dateFormate: String {
        case MMMMdyyyy = "MMMM d, yyyy"
    }
    
    //MARK: - Titles
    enum titles: String {
        case Profile
        case EditProfile = "Edit profile"
    }
    
    //MARK: - Prefixes
    enum prefix: String {
        case livesIn = "Lives in "
        case bornOn = "Born on "
        case fromCity = "From "
        case studiedAt = "Studied at "
        case phoneNumber = "+380 "
        case phoneNumberLabel = "+380"
    }
    
    //MARK: - Keys
    enum keys: String {
        case userKey
    }
}


