//
//  Extension+UITextField.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/7/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

extension UITextField {
    
    //MARK: - Prefix
    func createPrefix() {
        let prefixLabel = UILabel()
        prefixLabel.text = Constants.prefix.phoneNumberLabel.rawValue
        prefixLabel.sizeToFit()
        
        self.leftView = prefixLabel
        self.leftViewMode = .always

    }
}
