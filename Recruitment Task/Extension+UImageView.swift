//
//  Extension+UImageView.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/6/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

extension UIImageView {
    
    //MARK: - CornerRadius
    func setRadius() {
        layer.cornerRadius = frame.width / 2.0
        layer.masksToBounds = true
    }
    
    //MARK: - Download image
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        DispatchQueue.global().async { [weak self] in
            self?.getData(from: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async() {
                    self?.image = UIImage(data: data)
                }
            }
        }
        
    }
}
