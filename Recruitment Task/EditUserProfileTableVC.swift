//
//  EditUserProfileTableVC.swift
//  Recruitment Task
//
//  Created by Bohdan Hodovanets on 7/6/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

class EditUserProfileTableVC: UITableViewController {

    @IBOutlet weak var livesInTextField: UITextField!
    @IBOutlet weak var bornOnTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var studiedAtTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var biographyTextView: UITextView!
    
    private var user = User()
    private let storage = StorageManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIElements()
    }
    
    //MARK: - Navigation
    private func createSaveBarItem() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save,
                                         target: self,
                                         action: #selector(tappedSaveButton(sender:)))
        
        navigationItem.title = Constants.titles.EditProfile.rawValue //Set tittle for Edit User Profile view
        navigationItem.rightBarButtonItem = saveButton
    }
    
    @objc func tappedSaveButton(sender: UIBarButtonItem) {
        if checkPhoneNumber() {
            prepearModelForSaving(model: user)
            setDataToStorage(user: user)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    private func checkPhoneNumber() -> Bool {
        if phoneNumberTextField.text?.count == 0 || phoneNumberTextField.text?.count == Constants.maxLengthPhoneNumber {
            return true
        } else {
            return false
        }
    }
    
    //MARK: - Setup UI Elements
    private func setupUIElements() {
        setTextFieldDelegate()
        createSaveBarItem()
        createDoneBarButton()
        setDataPicker()
        configureData(with: getDataFromStorages())
        
        phoneNumberTextField.createPrefix()
    }
    
    //MARK: - Keybroard Done Bar Button
    private func createDoneBarButton() {
        let toolBar = UIToolbar()
        let fixebleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
        
        toolBar.sizeToFit()
        toolBar.setItems([fixebleSpace, doneButton], animated: false)
        
        bornOnTextField.inputAccessoryView = toolBar
        phoneNumberTextField.inputAccessoryView = toolBar
        biographyTextView.inputAccessoryView = toolBar
    }
    
    @objc func doneClicked() {
        if limitCount(of: phoneNumberTextField,
                      min: Constants.minLengthPhoneNumber,
                      max: Constants.maxLengthPhoneNumber) {
            tableView.endEditing(true)
        }
    }
    
    //MARK: - Date Picker
    private func setDataPicker() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        bornOnTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormate.MMMMdyyyy.rawValue
        bornOnTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.editUserProfileRows
    }
   
    //MARK: - Storage
    private func setDataToStorage(user: User) {
        let encoder = JSONEncoder()
        if let encodedUser = try? encoder.encode(user) {
            storage.set(value: encodedUser, key: Constants.keys.userKey)
        }
    }
    
    private func getDataFromStorages() -> User? {
        let decoder = JSONDecoder()
        
        guard let savedUser = storage.get(key: Constants.keys.userKey) as? Data else { return nil }
        if let user = try? decoder.decode(User.self, from: savedUser) {
            return user
        } else {
            return nil
        }
    }
    
    private func configureData(with user: User?) {
        guard let user = user else { return }
        livesInTextField.text = user.livesIn
        bornOnTextField.text = user.bornOn
        fromTextField.text = user.from
        studiedAtTextField.text = user.studiedAt
        phoneNumberTextField.text = user.phoneNumber
        biographyTextView.text = user.biography
    }

    private func prepearModelForSaving(model: User) {
        guard let livesInTextField = livesInTextField.text,
              let bornOnTextField = bornOnTextField.text,
              let fromTextField = fromTextField.text,
              let studiedAtTextField = studiedAtTextField.text,
              let phoneNumberTextField = phoneNumberTextField.text,
              let biographyTextView = biographyTextView.text else { return }
        
        model.livesIn = livesInTextField
        model.bornOn = bornOnTextField
        model.from = fromTextField
        model.studiedAt = studiedAtTextField
        model.phoneNumber = phoneNumberTextField
        model.biography = biographyTextView
    }
}

//MARK: - TextField Delegate
extension EditUserProfileTableVC: UITextFieldDelegate {
    private func setTextFieldDelegate() {
        let textFields = [livesInTextField, fromTextField, phoneNumberTextField, studiedAtTextField]
        for i in 0..<textFields.count {
            textFields[i]?.delegate = self
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func limitCount(of textField: UITextField, min: Int, max: Int) -> Bool {
        if textField.isEditing && textField.text!.count < max &&
            textField.text!.count > min || textField.text!.count > max {
            return false
        } else {
            return true
        }
    }
}
